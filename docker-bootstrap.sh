# This script is launched at container startup 
# to substitute environment variables in app.js
# before running nginx

TEMPLATE_PATH=/usr/share/nginx/template
HTML_PATH=/usr/share/nginx/html

# overwrite running html with template html
copy_template(){
  echo ">> Copying template ($TEMPLATE_PATH) to $HTML_PATH"
  rm -rf $HTML_PATH
  cp -r $TEMPLATE_PATH $HTML_PATH
}

# find out VUE_APP... variables in app.js and replace it by corresponding env var
substitute_envvars(){
  echo -e ">> Substituting environment variables in $HTML_PATH \n"

  JS_APP_PATH=$(ls $HTML_PATH/js/app*.js)
  VARIABLES=$(grep -o 'VUE_APP_[A-Z0-9_]*' $JS_APP_PATH | sed 's|VUE_APP_||g' | sort | uniq)
  for variable in $VARIABLES
  do
    value=$(env | grep "$variable=" | sed "s|$variable=||g")
    echo $variable $value
    sed -i "s|VUE_APP_$variable|$value|g" $JS_APP_PATH
  done

  echo -e "\n>> Variables substitution done successfully!"
}

# run nginx in foreground
start_nginx(){
  nginx -g "daemon off;"
}

# "rebuild" app with env vars in $HTML_PATH then start nginx
copy_template
substitute_envvars
start_nginx
