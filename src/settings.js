
export default {
    octoauthBaseURL: process.env.VUE_APP_AUTHORIZATION_SERVER_URL,
    octoauthClientId: process.env.VUE_APP_OCTOAUTH_CLIENT_ID,
    octoauthScope: process.env.VUE_APP_OCTOAUTH_SCOPES,
    fileserverBaseUrl: process.env.VUE_APP_FILE_SERVER_URL
}
